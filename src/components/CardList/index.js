import React from "react";
import s from "./CardList.module.css";
import Card from "../Card/";

class CardList extends React.Component {
	hendleAddWord(word, wordTranslate) {

	}

	render() {

		const {children, item} = this.props;

		return (
		<div className={s.main}>
			{children}
			<div className={s.cardList}>
		        {item.map(({eng, rus}, index) => <Card key={index} eng={eng} rus={rus}/>)}
			</div>

		</div>
	);
	}
}

export default CardList;