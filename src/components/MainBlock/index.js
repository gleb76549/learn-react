import React from 'react';
import s from "./MainBlock.module.css";
import {ReactComponent as ReactLogoSvg} from "../../logo.svg";

const HeaderBlock = ({hideBackground = false, children}) => {
	const styleCover = hideBackground ? {backgroundImage: "none"} : {};
	return (
		<div id="main"className={s.cover} style={styleCover}>
			<div className={s.wrap}>
				{children}
			</div>
		</div>
	);
};

export default HeaderBlock;