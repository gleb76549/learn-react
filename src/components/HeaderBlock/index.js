import React from "react";
import s from "./HeaderBlock.module.css";
import {HomeOutlined} from '@ant-design/icons';
const HeaderBlock = () => {
	return (
		<>
			<div className={s.headerBlock}>
					<HomeOutlined style={{fontSize: "30px", color: "blue"}}/>
					<h1>React Maraphon Project</h1>
			</div>
		</>
	);
};

export default HeaderBlock;