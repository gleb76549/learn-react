import React from "react";
import s from "./FooterBlock.module.css";

const FooterBlock = () => {
	return (
		<footer>
			<div>
				<span>Developed with React support</span><span>gleb76549@gmail.com</span><span>telegram: @gleb45</span>
			</div>
		</footer>
	);
};

export default FooterBlock;