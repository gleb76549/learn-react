import React from 'react';
import MainBlock from "./components/MainBlock/";
import Header from "./components/Header";
import Paragraph from "./components/Paragraph";
import {ReactComponent as ReactLogoSvg} from "./logo.svg";
import CardList from "./components/CardList";
// import FooterBlock from "./components/FooterBlock";
import wordsList from "./components/wordsList.js";
import s from "./index.module.css";
import cl from "classnames";
import HeaderBlock from "./components/HeaderBlock/";
import {StepBackwardOutlined} from "@ant-design/icons";
class App extends React.Component {
    state = {
        done: false,
    }

    handleClickLearn() {
        this.setState({
            done: this.state.done ? false : true,
        })
    }

    render() {
        const {done} = this.state;
        const CardListStyle = done ? {} : {display: "none"};
        return (
        <>
            <HeaderBlock/>
            <div className={s.wrap}>
                <div className={cl(s.card, {[s.done]: done})}>
                    <div className={s.front}>
                        <MainBlock>
                            <Header>
                              Учите слова онлайн
                            </Header>
                            <button onClick={() => this.handleClickLearn()} style={{padding: "10px 30px", fontSize: "23px"}}>Учить</button>
                            <Paragraph>
                              Используйте карточки для запоминания и пополняйте активный словарный запас.
                            </Paragraph>
                        </MainBlock>
                    </div>
                    <div className={s.back}>
                        <div style={CardListStyle}>
                        <CardList item={wordsList}>
                            <button onClick={() => this.handleClickLearn()}><StepBackwardOutlined style={{fontSize: "40px"}}/></button>
                            <h1>Начать учить английский просто</h1>
                            <p>Кликайте по карточкам и узнавайте новые слова, быстро и легко</p>
                        </CardList>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{height: "400px"}}></div>
        </>
        );
    }
}

export default App;
